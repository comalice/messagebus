from abc import abstractmethod, ABC
from multimethod import multimethod
import typing


class Message:
    subject: str
    message: str


class IProducer(ABC):

    @abstractmethod
    def send_message(self, message: Message):
        pass


class IConsumer(ABC):

    @abstractmethod
    def receive_message(self, message: str):
        pass

    @abstractmethod
    def callback(self, message: str):
        pass


class MessageBus:
    subjects: typing.Dict[object, list[callable]]

    def __init__(self):
        self.subjects = {}

    def send_message(self, subject: object, message: str):
        print(f"Sending message {message} to {subject}")
        for callback in self.subjects[subject]:
            callback(message)

    def subscribe(self, subject: str, callback: callable):
        self.add_subject(subject)
        self.subjects[subject].append(callback)

    @multimethod
    def add_subject(self, subject: str):
        if subject not in self.subjects:
            self.subjects[subject] = []

    @multimethod
    def add_subject(self, subject: object):
        if subject not in self.subjects:
            self.subjects[subject] = [subject.callback]


class Actor(IProducer, IConsumer):
    bus: MessageBus

    def __init__(self, bus: MessageBus):
        self.bus = bus

    def send_message(self, message: Message):
        pass

    def receive_message(self, message: str):
        print(f"{self} received message {message}")

    def callback(self, message: str):
        self.receive_message(message)


def main():
    bus = MessageBus()

    actor = Actor(bus)

    bus.add_subject(actor)

    while True:
        bus.send_message(actor, input("> "))


if __name__ == "__main__":
    main()
