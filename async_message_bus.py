import logging
import threading
import time
from abc import abstractmethod, ABC
from queue import Queue
import typing


logging.basicConfig(level=logging.DEBUG, format='%(relativeCreated)6d %(threadName)s %(message)s')


class Message:
    subject: object
    message: str

    def __init__(self, subject: object, message: str):
        self.subject = subject
        self.message = message

    def __repr__(self):
        return f"Message: subject: {self.subject}, message: {self.message}"

    def __str__(self):
        return self.__repr__()


class Subject:
    name: object
    # Each subscriber has their own queue.
    subscribers: typing.Dict[object, Queue]

    def __init__(self, name: object):
        self.subscribers = {}
        pass

    def subscribe(self, subscriber: object):
        self.subscribers[subscriber] = Queue()

    def post_message(self, message: Message):
        for subscriber, queue in self.subscribers.items():
            queue.put(message)

    def receive_message(self, subscriber: object):
        return self.subscribers[subscriber].get()


class IProducer(ABC):

    @abstractmethod
    def send_message(self, message: Message):
        pass


class IConsumer(ABC):

    @abstractmethod
    def receive_message(self, message: str):
        pass


class MessageBus:
    subjects: typing.Dict[object, Subject]

    def __init__(self):
        self.subjects = {}

    def send_message(self, message: Message):
        self.subjects[message.subject].post_message(message)

    def get_message(self, subject: object, subscriber: object) -> Message:
        return self.subjects[subject].receive_message(subscriber)

    def subscribe(self, subject: object, subscriber: object):
        self.add_subject(subject)
        self.subjects[subject].subscribe(subscriber)

    def add_subject(self, subject: object):
        if subject not in self.subjects:
            self.subjects[subject] = Subject(subject)


class Actor(IProducer, IConsumer):
    bus: MessageBus

    def __init__(self, bus: MessageBus):
        self.bus = bus

    def send_message(self, message: Message) -> None:
        pass

    def receive_message(self) -> Message:
        return self.bus.get_message(self, self)

    def run(self, signal) -> None:
        while signal:
            logging.debug(f"received message {self.receive_message()}")
            time.sleep(5)


def main():
    bus = MessageBus()

    actor = Actor(bus)
    bus.add_subject(actor)
    bus.subscribe(actor, actor)

    job = threading.Thread(target=actor.run, args=[True])
    job.start()

    while True:
        bus.send_message(Message(subject=actor, message=input("> ")))


if __name__ == "__main__":
    main()
