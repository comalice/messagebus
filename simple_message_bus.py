import typing


class MessageBus:
    subjects: typing.Dict[str, list[callable]]

    def __init__(self):
        self.subjects = {}

    def send_message(self, subject: str, message: str):
        for callback in self.subjects[subject]:
            callback(message)

    def subscribe(self, subject: str, callback: callable):
        if subject not in self.subjects:
            self.subjects[subject] = [callback]
        else:
            self.subjects[subject].append(callback)


def main():
    print("Hello, world.")

    bus = MessageBus()

    def callback(message: str):
        print(f"Received message '{message}'")

    bus.subscribe("1", callback)

    bus.send_message("1", "This is a message, 123")


if __name__ == "__main__":
    main()
